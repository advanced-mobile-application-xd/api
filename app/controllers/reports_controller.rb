class ReportsController < ApplicationController
  before_action :authorize_request
  
  def create
    @report = Report.new(report_params)
   
    @report.save
    render json: @report
  end
   
  private
    def report_params
      params.require(:location).permit(:lat, :long, :alt, :acc, :time, :tid, :bea, :sat, :spd, :bat, :prov, :plat, :plat_ver, :serial, :bea, :sat, :spd, :bat, :plat, :plat_ver, :serial)
      # params.require(:location).permit(:bea, :sat, :spd, :bat, :plat, :plat_ver, :serial, :bea, :sat, :spd, :bat, :plat, :plat_ver, :serial)# .require([])
    end
end
