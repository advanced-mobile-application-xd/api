class ConfigurationsController < ApplicationController
  def index
    render json: ::Configuration.all
  end

  def show  
    @conf = ::Configuration.find(params[:id])
    render json: @conf
  end

  def create
    @conf = ::Configuration.new(configuration_params)
    @conf.tracked_object_id = SecureRandom.uuid 
    @conf.token = JsonWebToken.encode(tracked_object_id: @conf.tracked_object_id)
    @conf.save
    render json: @conf
  end
 
  private
    # Using a private method to encapsulate the permissible parameters
    # is just a good pattern since you'll be able to reuse the same
    # permit list between create and update. Also, you can specialize
    # this method with per-user checking of permissible attributes.
    def configuration_params
      params.require(:configuration).permit(:name, :position_interval_in_minutes)
    end
end
