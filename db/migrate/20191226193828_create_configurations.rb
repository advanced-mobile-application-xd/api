class CreateConfigurations < ActiveRecord::Migration[6.0]
  def change
    create_table :configurations do |t|
      t.string :name
      t.string :token
      t.uuid :tracked_object_id
      t.numeric :position_interval_in_minutes

      t.timestamps
    end
  end
end
