class CreateReports < ActiveRecord::Migration[6.0]
  def change
    create_table :reports do |t|
      t.numeric :acc
      t.numeric :alt
      t.numeric :bea
      t.decimal :lat, {:precision=>10, :scale=>6}
      t.decimal :long, {:precision=>10, :scale=>6}
      t.string :prov
      t.numeric :spd
      t.integer :sat
      t.timestamp :time
      t.string :serial
      t.uuid :tid
      t.string :plat
      t.numeric :plat_ver
      t.numeric :bat
    end
  end
end
