class ChangePlatVerInReportsToString < ActiveRecord::Migration[6.0]
  def change
    change_column :reports, :plat_ver, :string
  end
end
